#include <iostream>
#include <fstream>
#include <string>
#include <random>
#include <filesystem>

// This code was written by Jakov Krčadinac

namespace fs = std::filesystem;

// Function to generate a random string of given length
std::string generateRandomString(size_t length) {
    const char charset[] = "AGCT";
    const size_t max_index = (sizeof(charset) - 1);
    std::string randomString;
    randomString.reserve(length);
    std::random_device rd;
    std::mt19937 generator(rd());
    std::uniform_int_distribution<> distribution(0, max_index - 1);
    
    for(size_t i = 0; i < length; ++i) {
        randomString += charset[distribution(generator)];
    }
    
    return randomString;
}

// Function to save the string to a new .txt file in a given folder
void saveStringToFile(const std::string& str, const std::string& folder, const std::string& filename) {
    // Ensuring that the folder exists
    fs::create_directories(folder);
    
    // Generating a unique file name
    static int fileCounter = 0;
    std::string path = folder + "/" + filename + ".txt";
    
    // Write the string to the file
    std::ofstream outFile(path);
    if(outFile) {
        outFile << str;
        outFile.close();
        std::cout << "String saved to " << path << std::endl;
    } else {
        std::cerr << "Error writing to file " << path << std::endl;
    }
}

int main() {
    // user gets to determine how long will the random string of nitrogen bases (A, G, C, T) be.
    // for this project we need random strings with the lengths of 10^3, 10^4, 10^5, 10^6 and 10^7  
    size_t length;
    std::cout << "Enter the length of the random string od nitrogen bases : ";
    std::cin >> length;
    
    
    // generate a random string of a given length
    std::string randomString = generateRandomString(length);
    

    // Create a name for the .txt file based on the string length
    std::string f_name = "Random_" + std::to_string(length);
    saveStringToFile(randomString, "Generated_testing_data", f_name);
    
    return 0;
}
