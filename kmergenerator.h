// Code written by Fran

#ifndef KMER_GENERATOR_H
#define KMER_GENERATOR_H

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class KmerGenerator {
public:
    KmerGenerator() {}
    ~KmerGenerator() {}

    static vector<string> createKmers(const string& sequence, int k);
};

#endif 