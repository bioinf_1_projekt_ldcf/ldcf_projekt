#include "cuckoo_filter.h"
#include "hashing.h"
#include <cmath>
#include <algorithm>
#include <iostream>
#include <string>
#include <bitset>
#include <random>
#include <utility> // for std::exchange

// Code written by Jakov & Fran

// This function calculates binary from a decimal number. 
// The length of the decimal number is determined by fingerprint_len, and it must not be larger than 128!!
std::string decimalToBinaryString(size_t decimalValue, size_t fingerprint_len) {
    // Convert decimal to binary
    std::string binaryString = std::bitset<128>(decimalValue).to_string(); // Assuming maximum fingerprint length is 64
    // Return a string with leading zeros if necessary
    return binaryString.substr(binaryString.size() - fingerprint_len);
}

// Constructor for the basic Cuckoo Filter
CuckooFilter::CuckooFilter(size_t num_buckets, size_t bucket_size, size_t fingerprint_size_arg)
    : num_buckets(num_buckets), bucket_size(bucket_size), fingerprint_size(fingerprint_size_arg) {
    table.resize(num_buckets);
}

// Helper function to compute the sha256 hash value of an item
size_t CuckooFilter::hash(const std::string& item) const {
    size_t hash_value = std::hash<std::string>{}(sha256(item));
    //std::cout << "Item: " << item << std::endl;
    //std::cout << "Hash value: " << hash_value<< std::endl;
    return hash_value;
}

// Helper function to compute the first index
size_t CuckooFilter::hash1(const std::string& item) const {
    size_t i1_hash = hash(item);
    return i1_hash % num_buckets;
}

// Helper function to compute the second index
size_t CuckooFilter::hash2(const std::string& item) const {
    size_t i1 = hash1(item);  // 
    std::string fing = fingerprint(item);
    size_t fing_hash = hash1(fing);
    return (i1 ^ fing_hash) % num_buckets;
}

// Helper function to compute the fingerprint
std::string CuckooFilter::fingerprint(const std::string& item) const {
    size_t hash_value = hash(item) % static_cast<size_t>(pow(2, fingerprint_size));
    
    std::string fing_binary_str = decimalToBinaryString(hash_value, fingerprint_size);

    // Print the fingerprint value before returning
    //std::cout << "Item: " << item << std::endl;
    //std::cout << "Hash value: " << hash1(item) << std::endl;
    //std::cout << "Fingerprint value: " << fing_binary_str << std::endl;

    return fing_binary_str;
}


// Function to insert an item into the filter
bool CuckooFilter::insert(const std::string& item) {
    auto fp = fingerprint(item);
    auto idx1 = hash1(item);
    auto idx2 = hash2(item);
    //std::cout << "Item: " << item << std::endl;
    //std::cout << "Hash value: " << idx1 << std::endl;
    //std::cout << "Fingerprint value: " << fp << std::endl;

    if (table[idx1].size() < bucket_size) {
        table[idx1].push_back(fp);
        return true;
    } else if (table[idx2].size() < bucket_size) {
        table[idx2].push_back(fp);
        return true;
    }

    // If both buckets are full, perform kick-out
    size_t i = (rand() % 2 == 0) ? idx1 : idx2; // Randomly pick one of the two buckets
    for (size_t n = 0; n < max_kicks; ++n) {
        fp = std::exchange(table[i][rand() % bucket_size], fp); // Swap fingerprint
        i = (i ^ hash1(fp)) % num_buckets; // Calculate new bucket index
        if (table[i].size() < bucket_size) {
            table[i].push_back(fp);
            return true;
        }
    }

    // if the for loop completes all iterations, the insert was NOT successful
    // we need to add the victim to the next layer int the LDCF
    victim = fp;
    //std::cout << "Victim is : " << victim << std::endl;

    //return relocate(idx1, 0);
    return false;
}

// Function to lookup an item in the filter
bool CuckooFilter::lookup(const std::string& item) const {
    auto fp = fingerprint(item);
    //std::cout << "Lookup item: " << item << std::endl;
    //std::cout << "Fingerprint value: " << fp << std::endl;
    auto idx1 = hash1(item);
    auto idx2 = hash2(item);
    
    return std::find(table[idx1].begin(), table[idx1].end(), fp) != table[idx1].end() ||
           std::find(table[idx2].begin(), table[idx2].end(), fp) != table[idx2].end();
}

std::string CuckooFilter::printBucketOccupancy() const {
    int sum = 0;
    
    for (const auto& bucket : table) {
            //for (const auto& entry : bucket) {
            //    std::cout << entry << " ";
            //}
            //std::cout << bucket.size() << std::endl;
            sum = sum + bucket.size();
    }
    //std::cout << "Broj pupunjenih bucketa: " << sum << " / " << bucket_size * num_buckets <<std::endl;
    std::string retStr = "Number of filled buckets: " + std::to_string(sum)  + " / " + std::to_string(bucket_size * num_buckets);
    return retStr;
}

