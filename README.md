In this project we implemented the Cuckoo filter algorithm as well as the Logarithmic Dynamic Cuckoo Filter (LDCF) based on the scientific paper "The Logarithmic Dynamic Cuckoo Filter" by F. Zhang, H. Chen, H. Jin and P. Reviriego in 2021. 
Other works that we referenced in our project include:
- Gaia ASC, de Sá PHCG, de Oliveira MS, Veras AAO. NGSReadsTreatment - A Cuckoo Filter-based Tool for Removing Duplicate Reads in NGS Data. Sci Rep. 2019 Aug 12
- H. Chen, L. Liao, H. Jin and J. Wu, The dynamic cuckoo filter, 2017 IEEE 25th International Conference on Network Protocols (ICNP), Toronto, ON, Canada, 2017
- B. Fan, D. Andersen, M. Kaminsky, M. Mitzenmacher, Cuckoo Filter: Practically Better Than Bloom; Carnegie Mellon University, Intel Labs, Harvard University; Proceedings of the 10th ACM International on Conference on emerging Networking Experiments and Technologies (2014)

C++ was used for all the coding, and we tested the model on both generated data and the E. Coli genome available on the EnsemblBacteria website. 

Project documentation and PowerPoint presentation are available in the Documentation & PPT folder.
