//Written by Fran
#include "HashNumber.h"
#include <sstream>
#include <iomanip>

uint16_t HashNumber::convertToNumber(const std::string& str) {
    // Convert a string to a number using a hash function
    std::hash<std::string> hasher;
    return hasher(str);
}

std::string HashNumber::convertFromNumber(uint64_t number) {
    // Convert a number back to a string (simple hex representation for demo)
    std::stringstream ss;
    ss << std::hex << number;
    return ss.str();
}
