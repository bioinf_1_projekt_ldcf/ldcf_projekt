// Written by Fran
// Code to test k-mer generation functionality


#include "kmergenerator.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <tuple>

// Helper function to print k-mers to a file stream
void outputKmers(ofstream& outFile, const vector<string>& kmers) {
    outFile << "[";
    if (!kmers.empty()) {
        for (size_t i = 0; i < kmers.size(); i++) {
            if (i > 0) outFile << ", ";
            outFile << "\"" << kmers[i] << "\"";
        }
    }
    outFile << "]";
}

// Test function for k-mer generation that outputs results to a file
void testKmerGeneration() {
    ofstream outFile("kmergenerator_results.txt");  // File to write the results
    if (!outFile.is_open()) {
        cerr << "Error opening file for output.\n";
        return;
    }

    outFile << "Testing the k-mer generation function:\n\n";

    vector<tuple<string, int, vector<string>>> testCases = {  //3 test cases, 2 edge cases
        {"ATCGATCG", 3, {"ATC", "TCG", "CGA", "GAT", "ATC", "TCG"}}, 
        {"GATTACA", 7, {"GATTACA"}},
        {"GATT", 10, {}}
    };

    for (auto& [sequence, k, expectedKmers] : testCases) {
        auto generatedKmers = KmerGenerator::createKmers(sequence, k);
        outFile << "Input Sequence: \"" << sequence << "\", k = " << k << "\n";
        outFile << "Expected K-mers: ";
        outputKmers(outFile, expectedKmers);
        outFile << "\nGenerated K-mers: ";
        outputKmers(outFile, generatedKmers);
        outFile << "\n\n";
    }

    outFile.close();  // Close the file stream
}

int main() {
    testKmerGeneration();
    return 0;
}
