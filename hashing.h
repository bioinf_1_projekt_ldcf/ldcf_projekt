#ifndef HASHING_H
#define HASHING_H

#include <string>

// Function to compute SHA256 hash using OpenSSL EVP
std::string sha256(const std::string& data);

#endif // HASHING_H
