// Written by Fran

#include "kmergenerator.h"

// Implement the k-mer generation function
vector<string> KmerGenerator::createKmers(const string& sequence, int k) {
    vector<string> kmers;
    // Return an empty vector if k is greater than the length of the sequence to avoid out_of_range error
    if (k > sequence.length() || k <= 0) {
        return kmers;
    }
    //iterates through the sequence and stores k-mers
    for (int i = 0; i <= sequence.length() - k; i++) {
        string kmer = sequence.substr(i, k);
        kmers.push_back(kmer);
    }
    return kmers;
}
