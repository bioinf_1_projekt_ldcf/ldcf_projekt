//Written by Fran
#ifndef HASHNUMBER_H
#define HASHNUMBER_H

#include <string>

class HashNumber {
public:
    static uint64_t convertToNumber(const std::string& str);
    static std::string convertFromNumber(uint64_t number);
};

#endif 
