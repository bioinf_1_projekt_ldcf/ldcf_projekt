#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <chrono>
#include <vector>
#include "cuckoo_filter.h"
#include <cmath>

// Code written by Jakov


// Function that loads data from a file and removes all '\n' characters
std::string DataLoader(const std::string& filePath) {
    std::ifstream inFile(filePath);

    // error message if file doesn't exist
    if (!inFile) {
        std::cerr << "Error opening file: " << filePath << std::endl;
        return "";
    }

    std::stringstream buffer;
    buffer << inFile.rdbuf();
    std::string fileContents = buffer.str();

    // erasing all the '\n' characters
    fileContents.erase(std::remove(fileContents.begin(), fileContents.end(), '\n'), fileContents.end());

    inFile.close();
    return fileContents;
}

std::vector<std::string> generateKmers(const std::string& sequence, int k) {
    std::vector<std::string> kmers;

    // Check if k is valid
    if (k <= 0 || k > sequence.length()) {
        std::cerr << "Invalid k value!" << std::endl;
        return kmers; // Return an empty vector
    }

    // Generate kmers
    for (size_t i = 0; i <= sequence.length() - k; ++i) {
        std::string kmer = sequence.substr(i, k);
        kmers.push_back(kmer);
    }

    return kmers;
}


// GLOBAL variables for CFs in CF_node class
size_t fingerprint_size = 32; //2^32
size_t bucket_size = 8;
size_t num_buckets = std::pow(2, 8);  // 2^8 = 256 

// GLOBAL variable for saving victims
std::vector<std::string> hacksaw_ridge;



// In this part of the code we establish classes needed for the LDCF -------------------------------------------------

class CF_Node {
public:
    // Cuckoo filter instance
    CuckooFilter cf =  CuckooFilter(num_buckets, bucket_size, fingerprint_size);
    bool isFull = false;
    std::string cf_node_victim_fp = "0";

    CF_Node* left; // pointer to left child CF_Node
    CF_Node* right;    // pointer to right child CF_Node

    CF_Node() : left(nullptr), right(nullptr) {
        //no other definitions are needed
    }

    // Function to check if element is successfully inserted in the CF node
    bool successful_insert(std::string element) {
        if (isFull){
            return false; // if CF is full don't try to insert new elements in it
        }
        if (cf.insert(element)) {
            return true;
        }
        else {
            isFull = true;
            // save the victim which will be added to the next layer in the LDCF
            cf_node_victim_fp = cf.victim;
            //std::cout << " We recieved victim : " << cf_node_victim_fp << std::endl;
            hacksaw_ridge.push_back(cf_node_victim_fp);
            return false;
        }
    }
    // Function to check if element exists in the CF node
    bool node_lookup(std::string element) {
        if (cf.lookup(element)){
            return true;
        } else {
            return false;
        }
    }

    std::string returnOccupancy(){
        return cf.printBucketOccupancy();
    }
};

class LDCF {
private:
    CF_Node* root;

    // We use this CF only for its fingerprint function, no elements are added to it
    CuckooFilter helper_cf =  CuckooFilter(num_buckets, bucket_size, fingerprint_size);


public:
    LDCF() : root(nullptr) {}

    // Recursive helper function for filling the CF_node with elements
    void insertHelper(CF_Node*& node, const std::string& element, int level) {
        if (node == nullptr) {
            node = new CF_Node();
        }

        if (node->successful_insert(element)) {
            // element is successfully inserted, CF is not full
        } else {
            // CF is full, first save the victim first by adding it to the new layer
            /*
            std::string victim_fgpt = node->cf_node_victim_fp;
            std::cout << "Adding victim " << victim_fgpt << " to level " << level+1 << std::endl;

            if (victim_fgpt[level] == '0') {
                insertHelper(node->left, victim_fgpt, level+1);
            } else {
                insertHelper(node->right, victim_fgpt, level+1);
            }
            */

            // insert the new element next
            std::string fgpt = helper_cf.fingerprint(element);  // get the fingerprint of the element for insertion to determine in which child will it be stored
            if (fgpt[level] == '0') {
                insertHelper(node->left, element, level+1);
            } else {
                insertHelper(node->right, element, level+1);
            }
        }
    }

    // actual function for inserting elements
    void insert(const std::string& str) {
        insertHelper(root, str, 0);
    }

    // Recursive helper function checking if element is in the LDCF
    bool lookupHelper(CF_Node*& node, const std::string& element, int level) {
        if (node == nullptr) {
            //std::cout << "U nullptru sam!" << std::endl;
            return false;
        }

        if (node->node_lookup(element)) {
            //std::cout << "Pretrazujem dubinu: " << level << std::endl;
            // element is in the CF Node
            return true;
        } else {
            // CF is full, check next level
            std::string fgpt = helper_cf.fingerprint(element);  // get the fingerprint of the element for insertion to determine in which child will it be stored
            if (fgpt[level] == '0') {
                return lookupHelper(node->left, element, level+1);
            } else {
                return lookupHelper(node->right, element, level+1);
            }
        }
        //return false;
    }

    // actual function for checking if element is in LDCF
    bool lookup(const std::string& str) {
        bool ldcf_lookup_result = lookupHelper(root, str, 0);

        if (ldcf_lookup_result) {
            return true;
        } else {
            std::string str_fgpt = helper_cf.fingerprint(str);
            auto it = std::find(hacksaw_ridge.begin(), hacksaw_ridge.end(), str_fgpt);

            if (it != hacksaw_ridge.end()) {
                // element was one of the victims
                return true;
            } else {
                // element is not found
                return false;
            }
        }
    }



    // function for printing the LDCF structure in a way that shows how free entries are in each CF node
    void printTree(CF_Node* node, int depth = 0) const {
        if (node == nullptr) return;

        for (int i = 0; i < depth; ++i) {
            std::cout << "  ";
        }
        std::cout << "[";
        
        std::cout << node->returnOccupancy();
        
        std::cout << "]" << std::endl;

        printTree(node->left, depth + 1);
        printTree(node->right, depth + 1);
    }

    // start printing from the root node
    void printTree() const {
        printTree(root);
    }


};


// End of the LDCF class definitions --------------------------------------------------------------------------------------------

int main() {

    // Randomly generated data for testing is stored in the 'Generated_testing_data' folder
    // E. coli genome data is stored in the 'E_coli_genome' folder
    std::string filePath = "E_coli_genome/E_coli_genome.txt";


    // Load the data for the LDCF 
    std::string data = DataLoader(filePath);

    // check the loaded data
    if (!data.empty()) {
        std::cout << "Data successfully loaded." << std::endl;
    } else {
        std::cout << "No data loaded." << std::endl;
    }

    //    DETERMINE the string size of E. coli genome to be tested
    data = data.substr(0, 100000); 
    std::cout << "Data length : " << data.length() << std::endl;


    // set the LDCF filter dimensions
    size_t fingerprint_size = 32; //2^32
    size_t bucket_size = 8;
    size_t num_buckets = std::pow(2, 8);  
    std::cout << "Number of entries per CF (num_buckets * bucket_size) : " << num_buckets * bucket_size << std::endl;

    // Generating Kmers from the loaded data
    int k = 20;
    std::vector<std::string> kmers = generateKmers(data, k);
    std::cout << "Kmer length : " << k << std::endl;

    // initializing the LDCF. Fingerprint size, bucket size and number of buckets in the CF nodes are already determined by the global variables.
    LDCF ldcf = LDCF();

    
    // START measuring time
    auto start_insert = std::chrono::high_resolution_clock::now();

    // inserting all kmers in the LDCF
    for (const auto& kmer : kmers) {
        ldcf.insert(kmer);
    }

    // STOP measuring time
    auto end_insert = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double> elapsed_insert = end_insert - start_insert;

    std::cout << "Elapsed time for INSERTION of all kmers: " << elapsed_insert.count() << " seconds" << std::endl;

    

    // print the LDCF node occupancy
    ldcf.printTree();

    bool neki_kmer_nije_u_filteru = false;
    int broj_prisutnih = 0;
    int broj_false_positiva = 0;
    int i = 0;

    // START measuring time
    auto start_lookup = std::chrono::high_resolution_clock::now();

    // helper CF object for using the fingerprint function in checking elements that are not present
    //CuckooFilter pomocni_CF = CuckooFilter(num_buckets, bucket_size, fingerprint_size);
    
    for (const auto& kmer : kmers) {
        i++;
        if (ldcf.lookup(kmer) == false) {
            neki_kmer_nije_u_filteru = true;
            //std::cout << "Kmer that is not present: " << kmer <<  std::endl;
            //std::cout << "Its fingerprint: " << pomocni_CF.fingerprint(kmer) <<  std::endl;
            //std::cout << "Iteration: " << i <<  std::endl;
            
        } else {
            broj_prisutnih = broj_prisutnih + 1;
        }
        /* -> THIS PART of the code is used for testing the numer of false positives in the LDCF
        std::string lazni_kmer = kmer;
        lazni_kmer.back() = '2';
        if (ldcf.lookup(lazni_kmer) == true) {
            broj_false_positiva = broj_false_positiva + 1;
        }
        */
    }

    // STOP measuring time
    auto end_lookup = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double> elapsed_lookup = end_lookup - start_lookup;

    std::cout << "Elapsed time for LOOKUP of all kmers: " << elapsed_lookup.count() << " seconds" << std::endl;
    
    //std::cout << "Number of false positives: " << broj_false_positiva << std::endl;


    

    if (neki_kmer_nije_u_filteru) {
            std::cout << "Some Kmers are not present. :(" <<  std::endl;
            std::cout << "Number of Kmers present: " << broj_prisutnih <<  std::endl;
            std::cout << "Number of Kmers that should be present: " << data.length() - k + 1 <<  std::endl;
    }
    else {
        std::cout << "All Kmers are present! :)" <<  std::endl;
    }

    //std::cout << "Number of false positives: " << broj_false_positiva << std::endl;


    // Test output the generated kmers
    /*
    std::cout << "Generated " << k << "-mers:" << std::endl;
    for (const auto& kmer : kmers) {
        std::cout << kmer << std::endl;
    }
    */

    /* CODE for testing CF ------------------------------------------------------------------------------------------------------------
    
    // Initialize the Cuckoo filter
    CuckooFilter CF = CuckooFilter(num_buckets, bucket_size, fingerprint_size);

    for (const auto& kmer : kmers) {
        //if (!CF.insert(kmer)){
        //    std::cout << "Filter je pun" <<  std::endl;  // OVO MAKNI
        //};
        //std::cout << "umecem " << kmer <<  std::endl;
        CF.insert(kmer);
    }


    std::cout << CF.printBucketOccupancy() <<  std::endl;

    bool neki_kmer_nije_u_filteru = false;
    int broj_false_positiva = 0;

    for (const auto& kmer : kmers) {
        if (CF.lookup(kmer) == false) {
            neki_kmer_nije_u_filteru = true;
        }
        //std::string lazni_kmer = kmer;
        //lazni_kmer.back() = '2';
        //if (CF.lookup(lazni_kmer) == true) {
        //    broj_false_positiva = broj_false_positiva + 1;
        //}
    }

    

    if (neki_kmer_nije_u_filteru) {
            std::cout << "neki od kmerova nije u filteru iako bi trebao biti :(" <<  std::endl;
    }
    else {
        std::cout << "svi kmerovi su u filteru :)" <<  std::endl;
    }

    //std::cout << "broj false positiva: " << broj_false_positiva << std::endl;
    

    // KRAJ LDCF koda

    end of code for testing CF ------------------------------------------------------------------------------------------------------------
    */ 
    
    // Calculate the time it took for the program to run
    

    return 0;
}


// to run the code run this in the Windows terminal
// g++ -o ldcf_test LDCF_Final_Tester.cpp cuckoo_filter.cpp hashing.cpp -I/ucrt64/include -L/ucrt64/lib -lcrypto -lssl