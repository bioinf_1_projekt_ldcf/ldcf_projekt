#ifndef CUCKOO_FILTER_H
#define CUCKOO_FILTER_H

#include <vector>
#include <list>
#include <string>

// Code written by Jakov & Fran

// Basic Cuckoo Filter class
class CuckooFilter {
protected:
    std::vector<std::vector<std::string>> table;
    size_t num_buckets;
    size_t bucket_size;
    size_t fingerprint_size;
    size_t max_kicks = 500;


public:

    std::string victim = "0";

    CuckooFilter(size_t num_buckets, size_t bucket_size, size_t fingerprint_size);

    virtual size_t hash(const std::string& item) const;
    virtual size_t hash1(const std::string& item) const;
    virtual size_t hash2(const std::string& item) const;
    virtual std::string fingerprint(const std::string& item) const;

    virtual bool insert(const std::string& item);
    virtual bool lookup(const std::string& item) const;
    std::string printBucketOccupancy() const;
};

#endif // CUCKOO_FILTER_H
